﻿using System;
using System.IO;
using System.Linq;
using System.Text;

namespace Lab5_OOP
{
    class ConsoleInteractor
    {
        Progression _progression = null;
        string _progressionType = "";
        int _progressionsAmount = 0;

        void ProcessProgression()
        {
            _progression.CalculateFeature();
            _progression.CalculateSum();
            if (_progressionType == "Exponential" && Math.Abs(_progression.Feature) < 1)
            {
                _progression.CalculateAbsoluteSum();
            }
        }

        void ReadProgressionFromFile(StreamReader reader)
        {
            int elementsAmount = 0;
            while (_progressionType.Equals(String.Empty))
            {
                _progressionType = reader.ReadLine();

                try
                {
                    if (_progressionType == "Linear")
                    {
                        elementsAmount = Convert.ToInt32(reader.ReadLine());
                        _progression = new LinearProgression(ReadMembers(elementsAmount, reader), elementsAmount);
                    }
                    else if (_progressionType == "Exponential")
                    {
                        elementsAmount = Convert.ToInt32(reader.ReadLine());
                        _progression = new ExponentialProgression(ReadMembers(elementsAmount, reader), elementsAmount);
                    }
                }
                catch (Exception exc)
                {
                    Console.WriteLine(exc.Message);
                }
            }
        }

        void ShowProgression()
        {
            StringBuilder memebersString = new StringBuilder();
            for(int counter = 0; counter < _progression.ElementsAmount; counter++)
            {
                memebersString.Append(Convert.ToString(_progression[counter]) + ' ');
            }
            Console.WriteLine(_progressionType);
            Console.WriteLine("Members: " + memebersString);
            Console.WriteLine("Sum: " + Convert.ToString(_progression.Sum));
            Console.WriteLine("Feature: " + Convert.ToString(_progression.Feature));
            if(_progressionType == "Exponential" && Math.Abs(_progression.Feature) < 1)
            {
                Console.WriteLine("Absolute sum: " + Convert.ToString(_progression.CalculateAbsoluteSum()) + '\n');
            }
            else
            {
                Console.Write('\n');
            }
        }

        double[] ReadSequence(StreamReader reader)
        {

            var resultingSequence = reader.ReadLine().Trim().Split(' ').Select(double.Parse).ToArray();
   
            return resultingSequence;
        }

        double[] ReadMembers(int elementsAmount, StreamReader reader)
        {
            var res = ReadSequence(reader);
            if(res.Length != elementsAmount)
            {
                throw new Exception("Wrong members amount");                    
            }

            return res;
        }

        void CreateProgression(int param, StreamReader reader)
        {
            double first, last;
            int elsAmount;
            double feature;
            int initMethod;


            try {
                if(param == 1)
                {
                    Console.Write("Choose init method (1 - members amount and members themselves, 2 - first member, their amount and feature\n " +
                        "3 - the first and the last members, members amount)\n>> ");
                    initMethod = Convert.ToInt32(Console.ReadLine());       
                    if (initMethod == 1)
                    {
                        Console.Write("Members amount: ");
                        elsAmount = Convert.ToInt32(Console.ReadLine());
                        Console.Write("Members: ");
                        _progression = new LinearProgression(ReadMembers(elsAmount, reader), elsAmount);
                    }
                    else if (initMethod == 2)
                    {
                        Console.Write("First member, members amount, featue: ");
                        var arr = ReadSequence(reader);
                        try
                        {
                            first = arr[0];
                            elsAmount = (int)arr[1];
                            feature = arr[2];
                        }catch(Exception exc)
                        {
                            throw exc;
                        }
                        _progression = new LinearProgression(first, elsAmount, feature);
                    }
                    else if (initMethod == 3)
                    {
                        Console.Write("First member, last member, members amount: ");
                        var arr = ReadSequence(reader);
                        try
                        {
                            first = arr[0];
                            last = arr[1];
                            elsAmount = (int)arr[2];
                        }
                        catch (Exception exc)
                        {
                            throw exc;
                        }
                        _progression = new LinearProgression(first, last, elsAmount);
                    }
                    _progressionType = "Linear";
                }
                else if(param == 2)
                {
                    Console.Write("Choose init method (1 - members amount and members themselves, 2 - first member, their amount and feature\n " +
                        "3 - the first and the second members, members amount)\n>> ");
                    initMethod = Convert.ToInt32(Console.ReadLine());
                    if (initMethod == 1)
                    {
                        Console.Write("Members amount: ");
                        elsAmount = Convert.ToInt32(Console.ReadLine());
                        Console.Write("Members: ");
                        _progression = new ExponentialProgression(ReadMembers(elsAmount, reader), elsAmount);
                    }
                    else if (initMethod == 2)
                    {
                        Console.Write("First member, members amount, featue: ");
                        var arr = ReadSequence(reader);
                        try
                        {
                            first = arr[0];
                            elsAmount = (int)arr[1];
                            feature = arr[2];
                        }
                        catch (Exception exc)
                        {
                            throw exc;
                        }
                        _progression = new ExponentialProgression(first, elsAmount, feature);
                    }
                    else if (initMethod == 3)
                    {
                        Console.Write("First member, second member, members amount: ");
                        var arr = ReadSequence(reader);
                        try
                        {
                            first = arr[0];
                            last = arr[1];
                            elsAmount = (int)arr[2];
                        }
                        catch (Exception exc)
                        {
                            throw exc;
                        }
                             
                        _progression = new ExponentialProgression(first, last, elsAmount);
                    }
                    _progressionType = "Exponential";
                }
                else
                {
                    throw new Exception("Invalid parameter");
                }
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }


        public void Run() {
            int nextAction = -1;
            Console.Write("How to init progression :\n1 - from file - automatic calculation (few possible)\n" +
                "2 - manualy, manual calculations, one progression in time\n\n");

            while (true)
            {
                Console.Write(">> ");
                StreamReader reader;
                try
                {
                    nextAction = Convert.ToInt32(Console.ReadLine());
                }
                catch(Exception exc)
                {
                    Console.WriteLine(exc.Message);
                }

                if(nextAction == 1)
                {
                    string filename;
                    Console.Write("Enter file name: ");
                    filename = Console.ReadLine();
                    reader = new StreamReader(filename);

                    try
                    {
                        _progressionsAmount = Convert.ToInt32(reader.ReadLine());
                        int counter = 0;

                        while (!reader.EndOfStream && counter < _progressionsAmount)
                        {
                            ReadProgressionFromFile(reader);
                            ProcessProgression();
                            ShowProgression();
                            _progressionType = "";
                            counter++;
                        }

                    }
                    catch (Exception exc)
                    {
                        Console.WriteLine(exc.Message);
                    }


                }
                else if (nextAction == 2)
                {
                    reader = new StreamReader(Console.OpenStandardInput());

                    Console.Write("Progression type (1 - Linear, 2 - Exponential): ");
                    try
                    {
                        nextAction = Convert.ToInt32(Console.ReadLine());
                        CreateProgression(nextAction, reader);
                    }
                    catch (Exception exc)
                    {
                        Console.WriteLine(exc.Message);
                    }

                    Console.Write("\nWhacha gonna doo?\n" +
                    "1 - Calculate progression sum\n" +
                    "2 - Calculate n-element (like n-word, but element)\n" +
                    "3 - Calculate feature\n" +
                    "0 - Go to main menu\n");
                    if (_progressionType == "Exponential" && _progression.FeatureStatus && Math.Abs(_progression.Feature) < 1)
                    {
                        Console.WriteLine("4 - Calculate inf decr sum\n");
                    }
                    Console.Write('\n');
                    
                    while (true)
                    {
                        Console.Write(">> ");
                        try
                        {
                            nextAction = Convert.ToInt32(Console.ReadLine());
                            if (nextAction == 0)
                            {
                                break;
                            }
                            else if (nextAction == 1)
                            {
                                Console.WriteLine("Sum is " + Convert.ToString(_progression.Sum) + '\n');
                            }
                            else if (nextAction == 2)
                            {
                                Console.Write("Element`s pos: ");
                                int num = Convert.ToInt32(Console.ReadLine());
                                Console.WriteLine(Convert.ToString(num) + "-element is " + Convert.ToInt32(_progression[num]));
                            }
                            else if (nextAction == 3)
                            {
                                Console.WriteLine("Feature is " + Convert.ToString(_progression.Feature));
                            }
                            else if (nextAction == 4 && _progressionType == "Exponential" && _progression.FeatureStatus)
                            {
                                Console.WriteLine("Inf decr sum is " + Convert.ToString(_progression.CalculateAbsoluteSum()));
                            }
                        }
                        catch(Exception exc)
                        {
                            Console.WriteLine(exc.Message);
                        }
                    }
                }
                else if (nextAction == 0)
                {
                    return;
                }
            }
        }
    }
}
